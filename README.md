Caroline LAVAURE - Dorian LEROY

INSTRUCTIONS:

Nous n'avons pas pu prendre le temps de développer une fonctionnalité de passage de paramètres afin de renseigner le nom du fichier xpdl à parser, aussi est-il nécessaire de le coller à la racine du projet. En cas d'utilisation d'un fichier xpdl autre que le fichier exemple, le nom du fichier doit être également renseigné dans la classe ViewGenerator.

Il existe deux points d'entrée à notre application. Le premier point d'entrée, la fonction main de la classe App, permet de générer les squelettes d'ihm sous forme de fichier fxml à partir du fichier xpdl choisi.
Le deuxième point d'entrée, la fonction main de la classe AppView, permet de tester le squelette d'IHM ainsi généré. Ici aussi nous n'avons pas eu le temps de développer de fonctionnalité permettant de choisir quelle IHM visualiser, il faut donc la rentrer à la main dans la classe AppView. Le fichier fxml ainsi choisi doit de plus se trouver dans le même répertoire que cette classe.

Malheureusement, nous n'avons également pas eu le temps de finaliser la gestion de l'activation/désactivation des boutons en fonction de l'état des données affichées. Nous devrions pour celà revoir notre stratégie de parsing du fichier xpdl afin d'associer un id pertinent aux boutons, afin de nous permettre de les retrouver pour les activer/désactiver en fonction de l'état actuel de la donnée selectionnée.
