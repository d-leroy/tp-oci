package main;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class ViewGenerator {

	private Parser parser;

	public ViewGenerator() {
		parser = new Parser("internal_command_service.xpdl", "view.fxml");
	}

	public void generateButtons() {
		Map<String, List<String>> activities = parser.getActivities();
		Map<String, List<String>> ids = parser.getActivitiesId();
		System.out.println(activities);
		activities.forEach((k,v) -> {
			Document res = parser.generateButtons(k, v);
			XMLOutputter sortie = new XMLOutputter(Format.getPrettyFormat());
			try {
				sortie.output(res, new FileOutputStream(new File("view-"+k+".fxml")));
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
		try {
			parser.exportTransition();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}