package main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;


public class ControllerView implements Initializable {

	private SAXBuilder sxb = new SAXBuilder();
	private Map<String,Map<String,String>> transitions = new HashMap<String,Map<String,String>>();
	private Map<String,Button> buttons = new HashMap<String,Button>();

	@FXML private TableView<Data> tableView;
	@FXML private Button create;
	@FXML private BorderPane pane;

	@FXML
	protected void buttonClicked(ActionEvent e) {
		Data data = tableView.getSelectionModel().getSelectedItem();
		String id = ((Button)e.getSource()).getId();
		if(data != null) {
			String newState = transitions.get(data.getState()).get(id);
			if(newState != null) {
				data.setState(newState);
			}
		}
	}

	@FXML
	private void handleCreate(ActionEvent e) {
		Data data = Data.getData();
		data.setState("start_process");
		tableView.getItems().add(data);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Document transitionsDoc;
		try {
			transitionsDoc = sxb.build(new File("transition-map.xml"));
			Element root = transitionsDoc.getRootElement();
			List<Element> listTransitions = root.getChildren();
			// Stocke les transitions sous la forme 'from' -> 'id' -> 'to'.
			for(Element t : listTransitions) {
				Map<String,String> idMap = transitions.get(t.getAttributeValue("id"));
				if(idMap == null) {
					idMap = new HashMap<String,String>();
				}
				idMap.put(t.getAttributeValue("id"), t.getAttributeValue("to"));
				transitions.put(t.getAttributeValue("from"), idMap);
			}

			((FlowPane) pane.getBottom()).getChildren().forEach((n) -> {if(n instanceof Button) {
				if(n.getId() != "create") {
					buttons.put(n.getId(), (Button) n);
				}
			}});

			buttons.forEach((k,p) -> {
				System.out.println(""+k+";"+p);
			});
			
			tableView.getSelectionModel().selectedItemProperty().addListener((c,d,e) -> {
				Data selected = tableView.getSelectionModel().getSelectedItem();
				Map<String,String> map = transitions.get(selected.getState());
				/* 
				 * Itère sur la map des buttons ayant pour état "from" l'état courant de l'objet sélectionné
				 * afin de les activer.
				 */
				if(map != null) {
					map.forEach((id,to) -> {
						System.out.println(""+id+";"+to);
						Button tmp = buttons.get(to);
						if(tmp != null) {
							tmp.disableProperty().set(false);
						}
					});
				}
				
				/* 
				 * Itère sur la map des buttons n'ayant pas pour état "from" l'état courant de l'objet sélectionné
				 * afin de les désactiver.
				 */
				transitions.forEach((from,m) -> {
					if(!from.equals(selected.getState())) {
						m.forEach((id,to) -> {
							Button tmp = buttons.get(id);
							if(tmp != null) {
								tmp.disableProperty().set(true);
							}
						});
					}
				});
			});
		} catch (JDOMException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


}