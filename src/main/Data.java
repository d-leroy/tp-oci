package main;

import javafx.beans.property.SimpleStringProperty;

public class Data {

	private SimpleStringProperty name = new SimpleStringProperty();
	private SimpleStringProperty state = new SimpleStringProperty();
	private static int n=0;
	
	private Data(String name) {
		this.name.set(name);
	}
	
	public static Data getData() {
		n++;
		return new Data("data_"+n);
	}
	
	public String getName() {
		return name.get();
	}

	public String getState() {
		return state.get();
	}

	public void setName(String name) {
		this.name.set(name);
	}
	
	public void setState(String state) {
		this.state.set(state);
	}
}
