package main;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.input.SAXBuilder;

public class Parser {

	private SAXBuilder sxb;
	private Document docXpdl;
	private Document docFxml;
	private Element racineXpdl;

	private Map<String, Map<String,String>> transitions;

	public Parser(String xpdl, String fxml) {
		sxb = new SAXBuilder();
		try {
			docXpdl = sxb.build(new File(xpdl));
			racineXpdl = docXpdl.getRootElement();
			docFxml = sxb.build(new File(fxml));
			transitions = getTransitions();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public Map<String,List<String>> getActivities() {
		Map<String, List<String>> res = new HashMap<String, List<String>>();
		Namespace ns = Namespace.getNamespace("xpdl", "http://www.wfmc.org/2008/XPDL2.1");
		Element processes = racineXpdl.getChild("WorkflowProcesses", ns);
		List<Element> listProcesses = processes.getChildren("WorkflowProcess", ns);
		for(Element e : listProcesses) {
			Element activities = e.getChild("Activities", ns);
			List<Element> listActivities = activities.getChildren("Activity", ns);
			for(Element a : listActivities) {
				Element restrictions = a.getChild("TransitionRestrictions", ns);
				if(restrictions != null) {
					List<Element> refs = restrictions.getChild("TransitionRestriction", ns)
							.getChild("Split", ns)
							.getChild("TransitionRefs", ns)
							.getChildren("TransitionRef", ns);
					Element performers = a.getChild("Performers", ns);
					if(performers != null) {
						List<Element> listPerformers = performers.getChildren("Performer", ns);
						for(Element p : listPerformers) {
							for(Element ref : refs) {
								List<String> tmp = res.get(p.getValue());
								if(tmp != null) {
									tmp.add(ref.getAttributeValue("Id"));
								}
								else {
									tmp = new ArrayList<String>();
									tmp.add(ref.getAttributeValue("Id"));
								}
								res.put(p.getValue(), tmp);	
							}

						}
					}
				} else {
					Attribute name = a.getAttribute("Name");
					if(name != null) {
						Element performers = a.getChild("Performers", ns);
						if(performers != null) {
							List<Element> listPerformers = performers.getChildren("Performer", ns);
							for(Element p : listPerformers) {
								List<String> tmp = res.get(p.getValue());
								if(tmp != null) {
									tmp.add(name.getValue());
								}
								else {
									tmp = new ArrayList<String>();
									tmp.add(name.getValue());
								}
								res.put(p.getValue(), tmp);
							}
						}
					}
				}
			}
		}
		return res;
	}

	public Map<String,List<String>> getActivitiesId() {
		Map<String, List<String>> res = new HashMap<String, List<String>>();
		Namespace ns = Namespace.getNamespace("xpdl", "http://www.wfmc.org/2008/XPDL2.1");
		Element processes = racineXpdl.getChild("WorkflowProcesses", ns);
		List<Element> listProcesses = processes.getChildren("WorkflowProcess", ns);
		for(Element e : listProcesses) {
			Element activities = e.getChild("Activities", ns);
			List<Element> listActivities = activities.getChildren("Activity", ns);
			for(Element a : listActivities) {
				Attribute id = a.getAttribute("Id");
				if(id != null) {
					Element performers = a.getChild("Performers", ns);
					if(performers != null) {
						List<Element> listPerformers = performers.getChildren("Performer", ns);
						for(Element p : listPerformers) {
							List<String> tmp = res.get(p.getValue());
							if(tmp != null) {
								tmp.add(id.getValue());
							}
							else {
								tmp = new ArrayList<String>();
								tmp.add(id.getValue());
							}
							res.put(p.getValue(), tmp);
						}
					}
				}
			}
		}

		return res;
	}

	public Map<String,String> getActivityPerformer() {
		Map<String, String> res = new HashMap<String, String>();
		Namespace ns = Namespace.getNamespace("xpdl", "http://www.wfmc.org/2008/XPDL2.1");
		Element processes = racineXpdl.getChild("WorkflowProcesses", ns);
		List<Element> listProcesses = processes.getChildren("WorkflowProcess", ns);
		for(Element e : listProcesses) {
			Element activities = e.getChild("Activities", ns);
			List<Element> listActivities = activities.getChildren("Activity", ns);
			for(Element a : listActivities) {
				Attribute id = a.getAttribute("Id");
				if(id != null) {
					Element performers = a.getChild("Performers", ns);
					if(performers != null) {
						List<Element> listPerformers = performers.getChildren("Performer", ns);
						for(Element p : listPerformers) {
							res.put(id.getValue(), p.getValue());
						}
					}
				}
			}
		}

		return res;
	}

	public Map<String,Map<String,String>> getTransitions() {
		Map<String,Map<String,String>> res = new HashMap<String,Map<String,String>>();
		Namespace ns = Namespace.getNamespace("xpdl", "http://www.wfmc.org/2008/XPDL2.1");
		Element processes = racineXpdl.getChild("WorkflowProcesses", ns);
		List<Element> listProcesses = processes.getChildren("WorkflowProcess", ns);
		for(Element e : listProcesses) {
			Element transitions = e.getChild("Transitions", ns);
			List<Element> listTransitions = transitions.getChildren("Transition", ns);
			for(Element t : listTransitions) {
				Map<String, String> idMap = res.get(t.getAttributeValue("From"));
				if(idMap == null) {
					idMap = new HashMap<String,String>();
				}
				idMap.put(t.getAttributeValue("Id"), t.getAttributeValue("To"));
				res.put(t.getAttributeValue("From"),idMap);
			}
		}

		return res;
	}

	public List<String> getParticipants() {
		List<String> res = new ArrayList<String>();
		Namespace ns = Namespace.getNamespace("xpdl", "http://www.wfmc.org/2008/XPDL2.1");
		Element participants = racineXpdl.getChild("Participants", ns);
		List<Element> listParticipants = participants.getChildren("Participant", ns);
		for(Element e : listParticipants) {
			Attribute name = e.getAttribute("Name");
			if(name != null) {
				res.add(name.getValue());
			}
		}
		return res;
	}

	public Document generateButtons(String role, List<String> labels) {
		Document res = docFxml.clone();
		Element root = res.getRootElement();
		Namespace ns = Namespace.getNamespace("fx","http://javafx.com/javafx/8");
		Element bottom = root.getChild("bottom", ns);
		Element flowpane = bottom.getChild("FlowPane", ns);
		for(String s : labels) {
			Element button = new Element("Button");
			button.setAttribute("text", s);
			button.setAttribute("id", s, ns);
			flowpane.addContent(button);
		}
		return res;
	}

	public void exportTransition() throws IOException {

		File file = new File("transition-map.xml");
		if (!file.exists()) {
			file.createNewFile();
		}		
		FileWriter fw = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bw = new BufferedWriter(fw);
		bw.write("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		bw.write("<transitions>\n");
		transitions.forEach((from,m) -> {
			if(m != null) {
				m.forEach((id,to) -> {
					try {
						bw.write("\t<transition from=\""+from
								+"\" id=\""+id
								+"\" to=\""+to+"\" />\n");
					} catch (Exception e) {
						e.printStackTrace();
					}
				});
			}

		});
		bw.write("</transitions>\n");
		bw.close();
	}
}